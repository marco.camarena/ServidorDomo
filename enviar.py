from socket import socket, error
from threading import Thread
import socket
import threading

class Enviar(threading.Thread):

	def __init__(self, clients, clients_lock, enviar):
		threading.Thread.__init__(self)
		self.clients = clients
		self.clients_lock = clients_lock
		self.enviar = enviar

	def run(self):
		while True:
			if self.enviar.empty() == False:
				x = self.enviar.get()

				with self.clients_lock:
					for c in self.clients:
						try:
							c.send(x.encode())

						except BrokenPipeError:
							pass


		
