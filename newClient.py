from socket import socket, error
from threading import Thread
import socket
import threading

class newClient(threading.Thread):

    def __init__(self, conn, addr, clients, clients_lock, enviar):
        threading.Thread.__init__(self)
        self.conn = conn
        self.addr = addr
        self.clients = clients
        self.clients_lock = clients_lock
        self.enviar = enviar

    def run(self):

        with self.clients_lock:
            self.clients.add(self.conn)

        while True:
            try:
                data = self.conn.recv(1024).decode()
                #SE CAMBIA EL MENSAJE RECIBIDO A MAYUSCULAS
                data = str(data).upper()

            except error:
                print("Se ha desconectado: " + str(self.addr))


                with self.clients_lock:
                    self.clients.discard(self.conn)

                break

            else:
                if data:
                    #SE IMPRIME EL MENSAJE RECIBIDO Y SE LE AGREGA UN SALTO
                    print("Recibido: " + str(data) + "\n")
                    data = data + "\n"

                    self.enviar.put(data)