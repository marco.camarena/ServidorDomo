from socket import socket, error
from threading import Thread
from newClient import *
from enviar import *
import queue

def main():

	host = "192.168.1.223"
	port = 4444	

	mySocket = socket.socket()
	mySocket.bind((host, port))
	mySocket.listen(0)

	clients = set()
	clients_lock = threading.Lock()
	enviar = queue.Queue()

	servidorEnvio = Enviar(clients, clients_lock, enviar)
	servidorEnvio.start()

	while True:
		conn, addr = mySocket.accept()

		nuevoCliente = newClient(conn, addr, clients, clients_lock, enviar)
		nuevoCliente.start()

		print("Se ha conectado: " + str(addr))

	return 0


if __name__ == '__main__':
	main()